package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	public ViewLeadPage() {
       PageFactory.initElements(driver, this);
	}

	//@FindBy(how = How.CLASS_NAME,using="decorativeSubmit") WebElement eleLogout;
	
	@FindBy(how=How.LINK_TEXT,using="Leads")WebElement eleLeads;
	
	public LeadsPage clickLeads() {
	//	WebElement eleLeads = locateElement("link", "Leads");
		click(eleLeads);
		return new LeadsPage(); 
	}


}

















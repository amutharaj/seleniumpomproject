 package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
       PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how = How.CLASS_NAME,using="smallSubmit") WebElement eleCreateButton;
	public CreateLeadPage enterCompanyName(String cName) {
		clearAndType(eleCompanyName, cName);
		return this; 
	}
	
	public CreateLeadPage enterFirstName(String fName) {
		clearAndType(eleFirstName, fName);	
		return this; 
	}
	
	public CreateLeadPage enterLastName(String lName) {
		clearAndType(eleLastName, lName);
		return this; 
	
	}
	
	public ViewLeadPage clickCreateLeadButton() {
		click(eleCreateButton);
		return new ViewLeadPage(); 
	
	}
	}
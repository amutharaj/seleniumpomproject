package com.framwork.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.CreateLeadPage;
import com.framework.pages.LoginPage;

public class TC001_Create_Lead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_Create_Lead";
		testDescription = "Create Lead";
		testNodes = "Leads";
		author = "Aravind";
		category= "Smoke";
		dataSheetName= "TC001";
}
	
	@Test(dataProvider="fetchData")
	public void login(String username, String password, String CompanyName, String firstname, String Lastname ) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickCrmSfa()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(CompanyName)
		.enterFirstName(firstname)
		.enterLastName(Lastname)
		.clickCreateLeadButton();
		
		
		
	}
	
	
}